# big-out

Generates PDF from slides based on big-presentation.

Dependencies:
- [Puppeteer](https://github.com/GoogleChrome/puppeteer)
- [pdf-merge](https://github.com/wubzz/pdf-merge) (requires `pdftk` command)

## Install

```sh
sudo apt install pdftk

git clone https://gitlab.com/jsek/big-out.git
npm install
```

See [pdf-merge](https://github.com/wubzz/pdf-merge) for tips about PDFtk installation on other operating systems

## Usage

Execute `index.js` with range of slides to print.

```sh
node index.js 0 10
```

## Alternatives

- [big-printer](https://github.com/tmcw-up-for-adoption/big-printer)