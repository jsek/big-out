const fs = require('fs');
const merge = require('pdf-merge');
const puppeteer = require('puppeteer');

// TODO: Readme.md
//  - with (graph LR) page1..n -(print)> pages/1..n.pdf -(pdftk:merge)-> out.pdf
/* TODO: Options:

    Required
    - range e.g. 1,2,5-8

    Internals
    - silent
    - parallel  [true/false]
    - delay e.g. 500
    - skip  e.g. 'clean merge'

    Defaults
    - out   default: first page title | 'out'
    - size  default: 1024 x 640 (px)

    - url   default: http://localhost
    - port  default: 8080
*/

function getOptions () {
    const rangeArg = process.argv
    .slice(process.argv.length - 2, process.argv.length)
    .map(x => parseInt(x, 10));

    console.log(`Range: ${rangeArg[0]}-${rangeArg[1]}`);

    return {
        range   : range.apply({},rangeArg),
        baseUrl : 'http://localhost',
        port    : 8080,
        delay   : 500,
    };
}

function clean () {
    try {
        fs.accessSync('pages');
    }
    catch (err) {
        fs.mkdirSync('pages');
    }

    return new Promise((resolve, reject) => {
        fs.readdir('pages', function(_, files) {
            files
            .map(file => `pages/${file}`)
            .forEach(filePath => {
                const stats = fs.statSync(filePath);
                if (stats.isFile()) {
                    fs.unlinkSync(filePath);
                }
            });
            resolve();
        });
    });
}

async function open (browser, url) {
    const page = await browser.newPage();
    await page.setViewport({width: 1024, height: 640});
    await page.waitForFunction('window.innerWidth > 1000');
    await page.emulateMedia('screen');
    await page.goto(url);
    return page;
}

async function print (page, number) {
    return await page.pdf({
        path: `pages/${number}.pdf`,
        printBackground: true,
        width: '1024 px',
        height: '640 px',
        margin: { top: 0, right: 0, bottom: 0, left: 0 }
    });
}

function mergePages (slidesRange) {
    return merge(
        slidesRange.map(i => `pages/${i}.pdf`),
        { output: `out.pdf` }
    );
}

function sleep (miliseconds) {
    return new Promise(next => { setTimeout(next, miliseconds) });
}

function range (start, stop) {
    return Array.apply(null, {length: stop - start + 1}).map((_,i) => start + i);
}

(async () => {
    const startTime = +(new Date());
    const options = getOptions();
    await clean();
    const browser = await puppeteer.launch();
    console.log(`Printing ${options.range.length} pages...`);
    await Promise.all(options.range
        .map(async function (i) {
            let page = await open(browser, `${options.baseUrl}:${options.port}/#${i}`); 
            await sleep(options.delay);
            await print(page, i);
        })
    );
    await browser.close();
    await mergePages(options.range);
    console.log(`Successfully generated: out.pdf`);
    console.log(`Time: ${((+(new Date()) - startTime)/1000).toFixed(1)} seconds`)
    await clean();
    fs.rmdirSync('pages');
})()
.catch(console.error);